# TD1

`pandoc -s --toc README.md --css=./github-pandoc.css -o README.html`

## lscpu

```
Architecture :                              x86_64
  Mode(s) opératoire(s) des processeurs :   32-bit, 64-bit
  Address sizes:                            39 bits physical, 48 bits virtual
  Boutisme :                                Little Endian
Processeur(s) :                             4
  Liste de processeur(s) en ligne :         0-3
Identifiant constructeur :                  GenuineIntel
  Nom de modèle :                           Intel(R) Core(TM) i3-8145U CPU @ 2.10GHz
    Famille de processeur :                 6
    Modèle :                                142
    Thread(s) par cœur :                    2
    Cœur(s) par socket :                    2
    Socket(s) :                             1
    Vitesse maximale du processeur en MHz : 3900,0000
    Vitesse minimale du processeur en MHz : 400,0000
Caches (sum of all):                        
  L1d:                                      64 KiB (2 instances)
  L1i:                                      64 KiB (2 instances)
  L2:                                       512 KiB (2 instances)
  L3:                                       4 MiB (1 instance)
```

## Produit matrice-matrice

### Permutation des boucles

```
make TestProductMatrix.exe && ./TestProductMatrix.exe 1024
```

et

```
./TestProductMatrix.exe 2048
```

ordre           | time(n=1024) | MFlops(n=1024) | MFlops(n=2048)
--------------- | ------------ | -------------- | --------------
i,j,k (origine) | 17.1325      | 125.346        | 64.3614
j,i,k           | 23.8404      | 90.0774        | 57.0485
i,k,j           | 62.3803      | 34.4257        | 35.5435
k,i,j           | 62.7887      | 34.2017        | 36.3662
j,k,i           | 2.08743      | 1028.77        | 970.484
k,j,i           | 1.58434      | 1355.44        | 1312.25

Le temps d'exécution est différent à cause du temps qu'il faut pour accéder à la mémoire.

On peut s'apercevoir que les deux dernières versions sont les plus rapides (avec l'ordre **j,k,i** et i **k,j,i**). Dans ce cas on change _i_ le plus souvent, c'est-à-dire on lit les matrices colonne par colonne.

Et comme les données dans cette implémentation sont stockées en colonnes, on est continu en mémoire (on ne fait pas de "sautes") et on utilise le cache de la manière la plus efficace. On est donc plus rapide en utilisant le même algorithme, mais juste en changeant l'ordre des boucles.

### OMP sur la meilleure boucle

```
make TestProductMatrix.exe && OMP_NUM_THREADS=8 ./TestProductMatrix.exe 1024
```

OMP_NUM | MFlops(n=512) | MFlops(n=1024) | MFlops(n=2048) | MFlops(n=4096)
------- | ------------- | -------------- | -------------- | --------------
1       | 1451.8        | 1299.48        | 1299.89        | 1178.24        |
2       | 1534.96       | 1268.69        | 1133.55        | 1315.87
3       | 1456.55       | 1041.91        | 998.97         | 1189
4       | 1558.9        | 1280.73        | 1155.02        | 1258.96
5       | 1489.69       | 1329.74        | 1287.93        | 1246.69
6       | 1579.62       | 1428.07        | 1290.37        | 1244.95
7       | 1535.12       | 1326.19        | 1235.22        | 1154.56
8       | 1591.36       | 1361.36        | 1210.29        | 1236.71

On ne s'aperçoit pas d'amélioration, surtout pour les dimensions plus grandes. Cela peut être lié au fait que le temps d'exécution est principalement le temps qu'il faut pour accéder à la mémoire et pas le temps du calcul (il n'y a pas d'opération lourde dans les calculations, par contre on lit et écrit en mémoire souvent). Dans ce cas la parallélisation du processus ne va pas donner d'amélioration de performance significative.

### Produit par blocs

```
make TestProductMatrix.exe && ./TestProductMatrix.exe [matrix_size] [block_size]
```

Par exemple :

```
make TestProductMatrix.exe && ./TestProductMatrix.exe 1024 32
```

Si `block_size` est 0 ou pas fourni, on fait la multiplication séquentielle.

szBlock        | MFlops(n=512) | MFlops(n=1024) | MFlops(n=2048) | MFlops(n=4096)
-------------- | ------------- | -------------- | -------------- | --------------
origine (=max) | 1450.46       | 1237.09        | 1222.58        | 1136.93
32             | 1290.2        | 1277.09        | 941.346        | 1080.17
64             | 1329.17       | 1389.58        | 1211.28        | 1328.84
128            | 1316.26       | 1446.88        | 1235.2         | 1205.34
256            | 1429.1        | 1526.49        | 1297.6         | 1156.52
512            | 1510.45       | 1464.22        | 1285.47        | 1273.72
1024           | 1452.41       | 1295.37        | 1340.99        | 1252.7

On peut remarquer la performance un peu plus élevée pour la taille du bloc de 128, 256 et 512 éléments. On va choisir la taille de 256 éléments pour les prochains tests.

### Bloc + OMP

```
make TestProductMatrix.exe && for i in $(seq 1 8); do elap=$(OMP_NUM_THREADS=$i ./TestProductMatrix.exe [matrix_size] [block_size] | grep "MFlops" | grep -Eo '[+-]?[0-9]+([.][0-9]+)?'); echo -e "$i\t$elap"; done > /dev/tty
```

szBlock        | OMP_NUM | MFlops(n=512) | MFlops(n=1024) | MFlops(n=2048) | MFlops(n=4096)
-------------- | ------- | ------------- | -------------- | -------------- | --------------
origine (=max) | 1       | 1491.13       | 1338.04        | 1260.94        | 1222.52        |
origine (=max) | 2       | 1545.94       | 1202.29        | 1261.59        | 1256.62        |
origine (=max) | 3       | 1582.03       | 1582.03        | 1262.53        | 1229.17        |
origine (=max) | 4       | 1571.34       | 1321.07        | 1381.42        | 1236.28        |
origine (=max) | 5       | 1591.44       | 1366.89        | 1340.64        | 1268.87        |
origine (=max) | 6       | 1578.91       | 1165.69        | 1315.48        | 1166.96        |
origine (=max) | 7       | 1563.89       | 1186.48        | 1279.6         | 1084.28        |
origine (=max) | 8       | 1568.09       | 1144.88        | 1351.04        | 1123.02        |
256            | 1       | 1463.58       | 1513.05        | 1100.09        | 1038.61        |
256            | 2       | 1510.61       | 1498.4         | 1033.98        | 1059.44        |
256            | 3       | 1509.75       | 1418.29        | 1005.07        | 981.073        |
256            | 4       | 1508.08       | 1443.44        | 1033.24        | 1138.26        |
256            | 5       | 1509.27       | 1294.3         | 1041.48        | 1079.17        |
256            | 6       | 1388.45       | 1427.33        | 1012.29        | 1203.43        |
256            | 7       | 1509.49       | 1506.53        | 1036.35        | 1050.12        |
256            | 8       | 1508.57       | 1376.93        | 1186.49        | 1105.87        |

La parallélisation donne une performance un peu plus élevée pour les dimensions des matrices plus petites (512, 1024 éléments). Pour les tailles des matrices plus grandes on s'aperçoit une certaine dégradation de la valeur de MFlops.

Dans le cas de la multiplication par blocs, chaque bloc calculé ne dépend pas des autres blocs et la parallélisation du processus est plus avantageuse que dans le cas de multiplication naïve.

### Comparaison with BLAS

```
make test_product_matrice_blas.exe && ./test_product_matrice_blas.exe 1024
```

Approche         | MFlops(n=512) | temps(n=512) | MFlops(n=1024) | temps(n=1024) | MFlops(n=2048) | temps(n=2048) | MFlops (n=4096) | temps(n=4096)
---------------- | ------------- | ------------ | -------------- | ------------- | -------------- | ------------- | --------------- | -------------
BLAS             | 1582.72       | 0.169604     | 1414.36        | 1.51834       | 1352.15        | 12.7056       | 1275.26         | 107.773       |
Naif             | 1404.9        | 0.19107      | 1251.55        | 1.71586       | 1382.98        | 12.4224       | 1248.85         | 110.052       |
Naif+OMP(4)      | 1492.15       | 0.179898     | 1347.07        | 1.59419       | 1272.88        | 13.4968       | 1272.25         | 108.028       |
Bloc(256)        | 1419.34       | 0.189127     | 1520.61        | 1.41225       | 1257.03        | 13.6671       | 1038            | 132.407       |
Bloc(256)+OMP(4) | 1441.45       | 0.186226     | 1523.51        | 1.40956       | 1187.74        | 14.4644       | 1066.88         | 128.823       |

Parmi toutes les approches, BLAS donne les meilleurs résultats (voir le tableau comparatif au-dessus).

# Tips

```
env 
    OMP_NUM_THREADS=4 ./produitMatriceMatrice.exe
```

```
$ for i in $(seq 1 4); do elap=$(OMP_NUM_THREADS=$i ./TestProductOmp.exe|grep "Temps CPU"|cut -d " " -f 7); echo -e "$i\t$elap"; done > timers.out
```
