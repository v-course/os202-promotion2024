#include <algorithm>
#include <iostream>
#include <thread>
#if defined(_OPENMP)
#include <omp.h>
#endif
#include "ProdMatMat.hpp"
#include <cassert>

namespace
{
  void prodSubBlocks(int iRowBlkA, int iColBlkB, int iColBlkA, int szBlock,
                     const Matrix &A, const Matrix &B, Matrix &C)
  {
    for (int k = iColBlkA; k < std::min(A.nbCols, iColBlkA + szBlock); k++)
      for (int j = iColBlkB; j < std::min(B.nbCols, iColBlkB + szBlock); j++)
        for (int i = iRowBlkA; i < std::min(A.nbRows, iRowBlkA + szBlock); ++i)
          C(i, j) += A(i, k) * B(k, j);
  }
  const int szBlock = 32;
} // namespace

Matrix operator*(const Matrix &A, const Matrix &B)
{
  Matrix C(A.nbRows, B.nbCols, 0.0);
  prodSubBlocks(0, 0, 0, std::max({A.nbRows, B.nbCols, A.nbCols}), A, B, C);
  return C;
}

Matrix multiplyByBlocks(const Matrix &A, const Matrix &B, const int blockSize)
{
  assert(A.nbRows == B.nbCols && A.nbRows == A.nbCols &&
         "Multiplication by blocks is only supported for square matrices");
  assert(A.nbRows % blockSize == 0 &&
         "Block size is incompatible with the matrix size");

  Matrix C(A.nbRows, B.nbCols, 0.0);

  const int matrixSize = A.nbRows; // both matrices are square

  for (int j = 0; j < matrixSize; j += blockSize)
    for (int i = 0; i < matrixSize; i += blockSize)
      for (int k = 0; k < matrixSize; k += blockSize)
        prodSubBlocks(i, j, k, blockSize, A, B, C);

  return C;
}
