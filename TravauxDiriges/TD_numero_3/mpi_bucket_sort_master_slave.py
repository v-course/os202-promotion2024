import numpy as np
import math
import sys
from time import time
from mpi4py import MPI

globCom = MPI.COMM_WORLD.Dup()
comm = MPI.COMM_WORLD
n_processes = globCom.size
rank = globCom.rank


STOP_TAG = MPI.TAG_UB

N_BUCKETS = 10


def flatten(array):
    return [x for xs in array for x in xs]


def get_bucket_index(value, minimal_value, interval_step, total_n_buckets):
    bucket_index = math.floor((value - minimal_value) / interval_step)
    if bucket_index is None:
        return 0

    bucket_index = max(0, bucket_index)
    bucket_index = min(bucket_index, total_n_buckets - 1)
    return bucket_index


def bucket_sort(array, n_buckets):
    if len(array) <= 2:
        return np.sort(array).tolist()

    n_buckets = min(len(array), n_buckets)

    max_v = np.max(array)
    min_v = np.min(array)

    interval_step = abs((max_v - min_v) / n_buckets)
    if interval_step == 0:
        return np.sort(array).tolist()

    buckets = list(map(lambda x: [], range(n_buckets)))

    for el in array:
        bucket_index = get_bucket_index(
            value=el,
            minimal_value=min_v,
            interval_step=interval_step,
            total_n_buckets=len(buckets),
        )
        buckets[bucket_index].append(el)

    for i, b in enumerate(buckets):
        buckets[i] = bucket_sort(np.copy(b).tolist(), n_buckets)

    return flatten(buckets)


def master():
    size = 0
    if len(sys.argv) > 1:
        size = int(sys.argv[1])

    if size <= 0:
        return []

    # Initialize array
    max_value = size
    min_value = 0
    array = np.random.randint(min_value, max_value, size=(size)).tolist()

    interval_step = abs((max_value - min_value) / N_BUCKETS)

    # Initialize buckets
    buckets = list(map(lambda _: [], range(N_BUCKETS)))

    # Fill buckets
    for el in array:
        bucket_index = math.floor((el - min_value) / interval_step)
        bucket_index = max(0, bucket_index)
        bucket_index = min(bucket_index, len(buckets) - 1)
        buckets[bucket_index].append(el)

    # Sort each bucket
    for i in range(1, n_processes):
        comm.send(buckets[i], dest=i, tag=i)

    for i in range(n_processes, len(buckets)):
        status = MPI.Status()
        data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
        buckets[status.Get_tag()] = data

        comm.send(obj=buckets[i], dest=status.Get_source(), tag=i)

    for i in range(1, n_processes):
        status = MPI.Status()
        data = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
        buckets[status.Get_tag()] = data

        comm.send(obj=None, dest=i, tag=STOP_TAG)

    return flatten(buckets)


def slave():
    # Each slave sorts one bucket using sequential bucket sort algorithm
    while True:
        status = MPI.Status()

        array = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
        if status.Get_tag() == STOP_TAG:
            break

        sorted = bucket_sort(array, N_BUCKETS)
        comm.send(obj=sorted, dest=0, tag=status.Get_tag())


if rank == 0:
    start_time = time()
    sorted_array = master()
    total_time = time() - start_time
    print(f"Temps du calcul : {total_time} secondes\n")
else:
    slave()
