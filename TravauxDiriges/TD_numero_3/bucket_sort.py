import numpy as np
import math
import sys
from time import time

N_BUCKETS = 10


def flatten(array):
    return [x for xs in array for x in xs]


def get_bucket_index(value, minimal_value, interval_step, total_n_buckets):
    bucket_index = math.floor((value - minimal_value) / interval_step)
    if bucket_index is None:
        return 0

    bucket_index = max(0, bucket_index)
    bucket_index = min(bucket_index, total_n_buckets - 1)
    return bucket_index


def bucket_sort(array, n_buckets):
    if len(array) <= 2:
        return np.sort(array).tolist()

    max_v = np.max(array)
    min_v = np.min(array)

    interval_step = abs((max_v - min_v) / n_buckets)
    if interval_step == 0:
        return np.sort(array).tolist()

    buckets = list(map(lambda _: [], range(n_buckets)))

    for el in array:
        bucket_index = get_bucket_index(
            value=el,
            minimal_value=min_v,
            interval_step=interval_step,
            total_n_buckets=len(buckets),
        )
        buckets[bucket_index].append(el)

    for i, b in enumerate(buckets):
        buckets[i] = bucket_sort(np.copy(b).tolist(), n_buckets)

    return flatten(buckets)


size = 1
if len(sys.argv) > 1:
    size = int(sys.argv[1])


array = np.random.randint(0, size, size=(size)).tolist()
start_time = time()
sorted = bucket_sort(array, N_BUCKETS)
print(f"\nTemps du calcul : {time() - start_time} secondes\n")
