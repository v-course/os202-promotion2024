# TD n°3 - parallélisation du Bucket Sort

Implémenter l'algorithme "bucket sort" tel que décrit sur les deux dernières planches du cours n°3 :

- le process 0 génère un tableau de nombres arbitraires,
- il les dispatch aux autres process,
- tous les process participent au tri en parallèle,
- le tableau trié est rassemblé sur le process 0.

## lscpu

```
Architecture :                              x86_64
  Mode(s) opératoire(s) des processeurs :   32-bit, 64-bit
  Address sizes:                            39 bits physical, 48 bits virtual
  Boutisme :                                Little Endian
Processeur(s) :                             4
  Liste de processeur(s) en ligne :         0-3
Identifiant constructeur :                  GenuineIntel
  Nom de modèle :                           Intel(R) Core(TM) i3-8145U CPU @ 2.10GHz
    Famille de processeur :                 6
    Modèle :                                142
    Thread(s) par cœur :                    2
    Cœur(s) par socket :                    2
    Socket(s) :                             1
    Vitesse maximale du processeur en MHz : 3900,0000
    Vitesse minimale du processeur en MHz : 400,0000
Caches (sum of all):
  L1d:                                      64 KiB (2 instances)
  L1i:                                      64 KiB (2 instances)
  L2:                                       512 KiB (2 instances)
  L3:                                       4 MiB (1 instance)
```

## Executer

### Version séquentielle

```
python3 bucket_sort.py [array_length]
```

### Version parallélisée (maître-esclave)

```
mpirun -n [number_of_processes] python3 ./mpi_bucket_sort_master_slave.py [array_length]
```

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./mpi_bucket_sort_master_slave.py [array_length]
```

## Résultats

Testé avec un tableau de la taille 200_000 eléments et numero de buckets 10.

| Nombre de processus | Temps (sans parallélisation) | Temps (parallélisation) | Speed-up (%) |
| ------------------- | ---------------------------- | ----------------------- | ------------ |
| 2                   | 2.427                        | 2.073                   | 117          |
| 3                   | 2.427                        | 1.761                   | 138          |
| 4                   | 2.427                        | 1.469                   | 165          |

On s'apercois une croissance importante de speed-up en fonction de nombre de processeurs.

![Temps d'exécution](Temps_execution.svg)
