"""
Le jeu de la vie
################
Le jeu de la vie est un automate cellulaire inventé par Conway se basant normalement sur une grille infinie
de cellules en deux dimensions. Ces cellules peuvent prendre deux états :
    - un état vivant
    - un état mort
A l'initialisation, certaines cellules sont vivantes, d'autres mortes.
Le principe du jeu est alors d'itérer de telle sorte qu'à chaque itération, une cellule va devoir interagir avec
les huit cellules voisines (gauche, droite, bas, haut et les quatre en diagonales.) L'interaction se fait selon les
règles suivantes pour calculer l'irération suivante :
    - Une cellule vivante avec moins de deux cellules voisines vivantes meurt ( sous-population )
    - Une cellule vivante avec deux ou trois cellules voisines vivantes reste vivante
    - Une cellule vivante avec plus de trois cellules voisines vivantes meurt ( sur-population )
    - Une cellule morte avec exactement trois cellules voisines vivantes devient vivante ( reproduction )

Pour ce projet, on change légèrement les règles en transformant la grille infinie en un tore contenant un
nombre fini de cellules. Les cellules les plus à gauche ont pour voisines les cellules les plus à droite
et inversement, et de même les cellules les plus en haut ont pour voisines les cellules les plus en bas
et inversement.

On itère ensuite pour étudier la façon dont évolue la population des cellules sur la grille.
"""

import pygame as pg
import numpy as np
import random
from lifegame_patterns import dico_patterns
from app import App
from grille import Grille

MAX_ITERATIONS = 20


if __name__ == "__main__":
    import time
    import sys

    pg.init()
    choice = "glider"
    if len(sys.argv) > 1:
        choice = sys.argv[1]
    resx = 800
    resy = 800
    if len(sys.argv) > 3:
        resx = int(sys.argv[2])
        resy = int(sys.argv[3])
    print(f"Pattern initial choisi : {choice}")
    print(f"resolution ecran : {resx,resy}")
    try:
        init_pattern = dico_patterns[choice]
    except KeyError:
        print("No such pattern. Available ones are:", dico_patterns.keys())
        exit(1)
    grid = Grille(*init_pattern)
    appli = App((resx, resy), grid)

    counter = 0
    avg_time = None
    while True and (
        MAX_ITERATIONS is not None
        and counter < MAX_ITERATIONS
        or MAX_ITERATIONS is None
    ):
        counter += 1
        # time.sleep(0.5) # A régler ou commenter pour vitesse maxi
        t1 = time.time()
        diff = grid.compute_next_iteration()
        t2 = time.time()
        appli.draw()
        t3 = time.time()

        if avg_time is None:
            avg_time = t2
        else:
            avg_time = (avg_time + t2) / 2

        for event in pg.event.get():
            if event.type == pg.QUIT:
                print(f"Average calculation time: {avg_time:2.2e} secondes")
                pg.quit()

        print(
            f"Temps calcul prochaine generation : {t2-t1:2.2e} secondes, temps affichage : {t3-t2:2.2e} secondes\r",
            end="",
        )

    print(f"Average calculation time: {avg_time}")
