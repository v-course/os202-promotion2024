# Lifegame

parallelisation:
one processor shows the image,
others calculate (divide the board between processes with overlapping)

## lscpu

```
Architecture :                              x86_64
  Mode(s) opératoire(s) des processeurs :   32-bit, 64-bit
  Address sizes:                            39 bits physical, 48 bits virtual
  Boutisme :                                Little Endian
Processeur(s) :                             4
  Liste de processeur(s) en ligne :         0-3
Identifiant constructeur :                  GenuineIntel
  Nom de modèle :                           Intel(R) Core(TM) i3-8145U CPU @ 2.10GHz
    Famille de processeur :                 6
    Modèle :                                142
    Thread(s) par cœur :                    2
    Cœur(s) par socket :                    2
    Socket(s) :                             1
    Vitesse maximale du processeur en MHz : 3900,0000
    Vitesse minimale du processeur en MHz : 400,0000
Caches (sum of all):
  L1d:                                      64 KiB (2 instances)
  L1i:                                      64 KiB (2 instances)
  L2:                                       512 KiB (2 instances)
  L3:                                       4 MiB (1 instance)
```

## Executer

### Version séquentielle

```
python3 lifegame.py [pattern*]
```

### Version parallélisée (maître-esclave)

```
mpirun -n [number_of_processes] python3 ./mpi_lifegame_master_slave.py [pattern*]
```

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./mpi_lifegame_master_slave.py [pattern*]

```

## Résultats

| Nombre de threads | Temps de calcul | Speedup     |
| ----------------- | --------------- | ----------- |
| 1                 | 1710182709      | 100,000000% |
| 2                 | 1710182678      | 100,000002% |
| 3                 | 1710182766      | 99,999997%  |
| 4                 | 1710182846      | 99,999992%  |
