import sys
from lifegame_patterns import dico_patterns
from mpi4py import MPI
from app import App
from grille import Grille
import numpy as np
import time


globCom = MPI.COMM_WORLD.Dup()
comm = MPI.COMM_WORLD
n_processes = globCom.size
rank = globCom.rank
n_tasks = n_processes

STOP_TAG = MPI.TAG_UB

MAX_ITERATIONS = 20


def compute_next_iteration(cells):
    ny = len(cells)
    nx = len(cells[0])

    next_cells = np.empty((ny, nx), dtype=np.uint8)
    for i in range(ny):
        i_above = (i + ny - 1) % ny
        i_below = (i + 1) % ny
        for j in range(nx):
            j_left = (j - 1 + nx) % nx
            j_right = (j + 1) % nx
            voisins_i = [i_above, i_above, i_above, i, i, i_below, i_below, i_below]
            voisins_j = [j_left, j, j_right, j_left, j_right, j_left, j, j_right]
            voisines = np.array(cells[voisins_i, voisins_j])
            nb_voisines_vivantes = np.sum(voisines)
            if cells[i, j] == 1:  # Si la cellule est vivante
                if (nb_voisines_vivantes < 2) or (nb_voisines_vivantes > 3):
                    next_cells[i, j] = (
                        0  # Cas de sous ou sur population, la cellule meurt
                    )
                else:
                    next_cells[i, j] = 1  # Sinon elle reste vivante
            elif (
                nb_voisines_vivantes == 3
            ):  # Cas où cellule morte mais entourée exactement de trois vivantes
                next_cells[i, j] = 1  # Naissance de la cellule
            else:
                next_cells[i, j] = 0  # Morte, elle reste morte.
    return next_cells


def split_into_parts(cells, n_parts, overlap=1):
    n_rows = len(cells) / n_parts

    parts = []

    for i in range(n_parts):
        low = int(max(i - overlap, 0))
        high = int(min((i + 1) * n_rows + overlap, len(cells)))
        parts.append(cells[low:high])

    return parts


def get_boundaries_for_part(cells, n_of_part, n_rows, overlap=1):
    low = int(max(n_of_part - overlap, 0))
    high = int(min((n_of_part + 1) * n_rows + overlap, len(cells)))
    return low, high


def get_part(cells, n_of_part, n_rows, overlap=1):
    low, high = get_boundaries_for_part(cells, n_of_part, n_rows, overlap)
    return cells[low:high]


def master():
    import pygame as pg

    pg.init()
    choice = "glider"
    if len(sys.argv) > 1:
        choice = sys.argv[1]

    resx = 800
    resy = 800
    if len(sys.argv) > 3:
        resx = int(sys.argv[2])
        resy = int(sys.argv[3])

    print(f"Pattern initial choisi : {choice}")
    print(f"resolution ecran : {resx,resy}")

    try:
        init_pattern = dico_patterns[choice]
    except KeyError:
        print("No such pattern. Available ones are:", dico_patterns.keys())
        exit(1)

    # Initialize grid and app (GUI)
    grid = Grille(*init_pattern)
    appli = App((resx, resy), grid)

    assert grid.dimensions[0] % n_tasks == 0
    n_rows_for_grid_part = int(grid.dimensions[0] / n_tasks)

    avg_time = None
    counter = 0
    while True and (
        MAX_ITERATIONS is not None
        and counter < MAX_ITERATIONS
        or MAX_ITERATIONS is None
    ):
        counter += 1
        t1 = time.time()

        for i in range(1, n_processes):
            grid_part = get_part(
                grid.cells,
                n_of_part=i,
                n_rows=n_rows_for_grid_part,
            )
            # Send part of the grid to one of the subprocesses to calculate next state
            comm.send(grid_part, dest=i, tag=i)

        for i in range(n_processes, n_tasks):
            status = MPI.Status()

            # Receive part of the grid from a subprocess
            grid_part = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
            n_of_part = status.Get_tag()
            low, high = get_boundaries_for_part(
                grid.cells, n_of_part, n_rows_for_grid_part
            )
            grid.cells[low:high] = grid_part

            # Send part of the grid to one of the subprocesses to calculate next state
            grid_part = get_part(
                grid.cells,
                n_of_part=i,
                n_rows=n_rows_for_grid_part,
            )
            comm.send(obj=grid_part, dest=status.Get_source(), tag=i)

        for i in range(1, n_processes):
            status = MPI.Status()

            grid_part = comm.recv(source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status)
            n_of_part = status.Get_tag()
            low, high = get_boundaries_for_part(
                grid.cells, n_of_part, n_rows_for_grid_part
            )
            grid.cells[low:high] = grid_part

        t2 = time.time()

        # Update GUI
        appli.draw()

        t3 = time.time()

        if avg_time is None:
            avg_time = t2
        else:
            avg_time = (avg_time + t2) / 2

        for event in pg.event.get():
            if event.type == pg.QUIT:
                # Send STOP tag to all subprocesses
                for i in range(1, n_processes):
                    comm.send(obj=None, dest=i, tag=STOP_TAG)
                print(f"Average calculation time: {avg_time:2.2e} secondes")
                pg.quit()
                MPI.Finalize()

        print(
            f"Temps calcul prochaine generation : {t2-t1:2.2e} secondes, temps affichage : {t3-t2:2.2e} secondes\r",
            end="",
        )

    print(f"Average calculation time: {avg_time}")


def slave():
    while True:
        status = MPI.Status()

        grid = comm.recv(source=0, tag=MPI.ANY_TAG, status=status)
        if status.Get_tag() == STOP_TAG:
            break

        comm.send(obj=compute_next_iteration(grid), dest=0, tag=status.Get_tag())


if rank == 0:
    master()
else:
    slave()
