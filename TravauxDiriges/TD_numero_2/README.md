# TD2

## lscpu

```
Architecture :                              x86_64
  Mode(s) opératoire(s) des processeurs :   32-bit, 64-bit
  Address sizes:                            39 bits physical, 48 bits virtual
  Boutisme :                                Little Endian
Processeur(s) :                             4
  Liste de processeur(s) en ligne :         0-3
Identifiant constructeur :                  GenuineIntel
  Nom de modèle :                           Intel(R) Core(TM) i3-8145U CPU @ 2.10GHz
    Famille de processeur :                 6
    Modèle :                                142
    Thread(s) par cœur :                    2
    Cœur(s) par socket :                    2
    Socket(s) :                             1
    Vitesse maximale du processeur en MHz : 3900,0000
    Vitesse minimale du processeur en MHz : 400,0000
Caches (sum of all):                        
  L1d:                                      64 KiB (2 instances)
  L1i:                                      64 KiB (2 instances)
  L2:                                       512 KiB (2 instances)
  L3:                                       4 MiB (1 instance)
```

## 1.2 Question du cours n°2

L'accélération maximale selon la loi d'Amdhal : S = 1 / (1 - p) = 10.

## 1.3 Ensemble de Mandelbrot

On a testé trois approches différentes : sans parallélisation (la version originale), avec parallélisation en utilisant la bibliothèque [multiprocessing](https://docs.python.org/3/library/multiprocessing.html) et avec parallélisation en utilisant [mpi4py](https://mpi4py.readthedocs.io/en/stable/).

### Executer

#### Version originale (sans parallélisation)

```
python3 ./mandelbrot.py
```

#### Parallel avec multiprocessing

```
python3 ./mandelbrot.py [number_of_processes]
```

#### Parallel avec mpi4py

```
mpirun -n [number_of_processes] python3 ./mpi_mandelbrot.py
```

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./mpi_mandelbrot.py
```

### Résultats

Nombre de processus et approche | Temps (sans parallélisation) | Temps (parallélisation) | Speed-up (%)
------------------------------- | ---------------------------- | ----------------------- | ------------
2 (multiprocessing)             | 6.112                        | 3.157                   | 193
3 (multiprocessing)             | 6.112                        | 3.570                   | 171
4 (multiprocessing)             | 6.112                        | 3.317                   | 184
2 (mpi4py)                      | 6.112                        | 3.173                   | 193
3 (mpi4py)                      | 6.112                        | 3.388                   | 180
4 (mpi4py)                      | 6.112                        | 3.216                   | 190

On peut s'apercevoir que le temps d'exécution avec parallélisation est beaucoup plus rapide que la version originale. On obtient plus de 170% de speed-up dans tous les cas, pour certains cas cette valeur monte jusqu'à 193%.

Les résultats obtenus par `multiprocessing` et `mpi4py` sont comparables. Cependant, on ne remarque pas de grande différence de temps selon le nombre de threads.

En plus, on obtient un temps un peu plus élevé pour 3 et 4 threads. Cela peut être lié à l' échelle du problème : peut-être que pour les tailles de l'image plus larges on s'apercevra de meilleures performances que dans ce cas-là (il est possible que l'on "perd" du temps en distribuant les ressources entre les threads, alors 2 threads sont relativement plus performantes ici).

![speed-ud plot](./Speed-up.svg)

### Maître–esclave

#### Executer

```
mpirun -n [number_of_processes] python3 ./mpi_mandelbrot_master_slave.py [number_of_fragments]
```

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./mpi_mandelbrot_master_slave.py [number_of_fragments]
```

#### Résultats

Nombre de processus | Nombre de fragments | Temps (sans parallélisation) | Temps (parallélisation) | Speed-up (%)
------------------- | ------------------- | ---------------------------- | ----------------------- | ------------
2                   | 4                   | 7.917                        | 7.720                   | 103
2                   | 8                   | 7.917                        | 9.175                   | 86
2                   | 16                  | 7.917                        | 9.612                   | 82
2                   | 32                  | 7.917                        | 9.672                   | 82
3                   | 4                   | 7.917                        | 9.641                   | 82
3                   | 8                   | 7.917                        | 5.761                   | 137
3                   | 16                  | 7.917                        | 6.762                   | 117
3                   | 32                  | 7.917                        | 4.310                   | 184
4                   | 4                   | 7.917                        | 6.488                   | 122
4                   | 8                   | 7.917                        | 6.532                   | 121
4                   | 16                  | 7.917                        | 5.737                   | 138
4                   | 32                  | 7.917                        | 6.622                   | 120

La nouvelle approche donne des performances plus élevées pour 3 ou 4 processus avec 8, 16 ou 32 tâches réparties entre les processus. En cas de 2 processus (= 1 esclave), la parallélisation de la tâche principale donne un temps d'exécution élevé.

![Maître–esclave. Nombre de processus: 2
](./master_slave_nbp_2.svg)

![Maître–esclave. Nombre de processus: 3
](./master_slave_nbp_3.svg)

![Maître–esclave. Nombre de processus: 4
](./master_slave_nbp_4.svg)

## 1.4 Produit matrice–vecteur

La taille de la matrice: 4000.

On compare le temps d'exécution avec parallélisation à la version originale :

```
python3 ./matvec.py
```

### Produit parallèle matrice – vecteur par colonne

```
mpirun -n [number_of_processes] python3 ./mpi_matvec_columns.py
```

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./mpi_matvec_columns.py
```

### Produit parallèle matrice – vecteur par ligne

```
mpirun -n [number_of_processes] python3 ./mpi_matvec_lines.py
```

`--hostfile` peut être utilisé :

```
mpirun --hostfile hostfile -n [number_of_processes] python3 ./mpi_matvec_lines.py
```

### Résultats

Nombre de processus et approche | Temps (sans parallélisation) | Temps (parallélisation) | Speed-up (%)
------------------------------- | ---------------------------- | ----------------------- | ------------
2 (par ligne)                   | 5.729                        | 2.852                   | 201
3 (par ligne)                   | 5.729                        | 3.792                   | 151
4 (par ligne)                   | 5.729                        | 4.216                   | 136
2 (par colonne)                 | 5.729                        | 4.324                   | 132
3 (par colonne)                 | 5.729                        | 3.777                   | 152
4 (par colonne)                 | 5.729                        | 5.642                   | 102

On s'aperçoit le temps minimal pour la version parallélisée avec la produit par ligne.

![Produit matrice-vecteur
](./Produit_matrice_vecteur.svg)
