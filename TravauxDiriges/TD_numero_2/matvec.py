# Produit matrice-vecteur v = A.u
import numpy as np
from time import time

start_time = time()

# Dimension du problème (peut-être changé)
dim = 4000
# Initialisation de la matrice
A = np.array([[(i + j) % dim + 1.0 for i in range(dim)] for j in range(dim)])
print(f"A = {A}")

# Initialisation du vecteur u
u = np.array([i + 1.0 for i in range(dim)])
print(f"u = {u}")

# Produit matrice-vecteur
v = A.dot(u)
total_time = time() - start_time
print(f"\nv = {v}")
print(f"\nTemps du calcul : {total_time} secondes\n")
