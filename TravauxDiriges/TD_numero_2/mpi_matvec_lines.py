import numpy as np
from mpi4py import MPI
from time import time

start_time = time()

# Dimension du problème (peut-être changé)
dim = 4000

globCom = MPI.COMM_WORLD.Dup()
comm = MPI.COMM_WORLD
n_processes = globCom.size
rank = globCom.rank

n_fragments = n_processes
lines_per_fragment = dim // n_fragments

n_of_fragment = rank

# Initialisation de la matrice
A_part = np.array(
    [
        [(i + j) % dim + 1.0 for i in range(dim)]
        for j in range(
            n_of_fragment * lines_per_fragment, (n_of_fragment + 1) * lines_per_fragment
        )
    ]
)

# Initialisation du vecteur u
u = np.array([i + 1.0 for i in range(dim)])

result_part = A_part.dot(u)
current_time = time() - start_time

gathered_result = comm.gather(result_part, root=0)
subprocesses_total_time = comm.gather(current_time, root=0)

if rank == 0:
    # main process
    start_time = time()
    gathered_result = np.hstack(gathered_result)
    gather_time = time() - start_time

    print(f"\nv = {gathered_result}")

    print(
        f"\nTemps du calcul : {np.max(subprocesses_total_time) + gather_time} secondes"
    )
