import numpy as np
from PIL import Image
import matplotlib.cm
from mpi4py import MPI
from mandelbrot_set import MandelbrotSet
from time import time


def get_convergence_fragment(
    fragment_width,
    fragment_height,
    mandelbrot_set,
    current_fragment=0,
):
    convergence = np.empty((fragment_width, fragment_height), dtype=np.double)

    scaleX = 3.0 / width
    scaleY = 2.25 / height

    y_offset = max(current_fragment * fragment_height - 1, 0)

    for y in range(y_offset, y_offset + fragment_height):
        for x in range(fragment_width):
            c = complex(-2.0 + scaleX * x, -1.125 + scaleY * y)
            convergence[x, y - y_offset] = mandelbrot_set.convergence(c, smooth=True)

    return convergence


def get_image_for_mandelbrot_convergence(convergence):
    return Image.fromarray(np.uint8(matplotlib.cm.plasma(convergence.T) * 255))


globCom = MPI.COMM_WORLD.Dup()
comm = MPI.COMM_WORLD
nbp = globCom.size
rank = globCom.rank

width, height = 1024, 1024
mandelbrot_set = MandelbrotSet(max_iterations=50, escape_radius=10)
lines_per_fragment = height // nbp

start_time = time()

convergence = get_convergence_fragment(
    fragment_width=width,
    fragment_height=lines_per_fragment,
    mandelbrot_set=mandelbrot_set,
    current_fragment=rank,
)

current_time = time() - start_time
gathered_convergence = comm.gather(convergence, root=0)
total_time = comm.gather(current_time, root=0)

if rank == 0:
    # main process
    print(f"Temps du calcul de l'ensemble de Mandelbrot : {np.max(total_time)}")

    start_time = time()
    gathered_convergence = np.hstack(gathered_convergence)
    image = get_image_for_mandelbrot_convergence(gathered_convergence)
    end_time = time()
    print(f"Temps de constitution de l'image : {end_time-start_time} secondes\n")

    image = image.convert("RGB")
    image.save("mandelbrot.jpg")
    image.show()
