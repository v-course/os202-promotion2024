import numpy as np
from PIL import Image
from time import time
import matplotlib.cm
from multiprocessing import Pool
from mandelbrot_set import MandelbrotSet
import sys


def get_convergence_fragment(
    fragment_width,
    fragment_height,
    mandelbrot_set,
    current_fragment=0,
    total_n_fragments=1,
):
    convergence = np.empty((fragment_width, fragment_height), dtype=np.double)

    scaleX = 3.0 / width
    scaleY = 2.25 / height

    y_offset = max(current_fragment * fragment_height - 1, 0)

    for y in range(y_offset, y_offset + fragment_height):
        for x in range(fragment_width):
            c = complex(-2.0 + scaleX * x, -1.125 + scaleY * y)
            convergence[x, y - y_offset] = mandelbrot_set.convergence(c, smooth=True)

    return convergence


def get_image_from_convergence_matrix(convergence):
    return Image.fromarray(np.uint8(matplotlib.cm.plasma(convergence.T) * 255))


def get_mandelbrot_image(width, height, max_iterations=50, escape_radius=10):
    mandelbrot_set = MandelbrotSet(max_iterations, escape_radius)

    # Calcul de l'ensemble de mandelbrot :
    start_time = time()
    convergence = get_convergence_fragment(width, height, mandelbrot_set)
    end_time = time()
    print(f"Temps du calcul de l'ensemble de Mandelbrot : {end_time-start_time}")

    # Constitution de l'image résultante :
    start_time = time()
    image = get_image_from_convergence_matrix(convergence)
    end_time = time()
    print(f"Temps de constitution de l'image : {end_time-start_time}")
    return image


def get_mandelbrot_image_parallel(
    width, height, n_threads=1, max_iterations=50, escape_radius=10
):
    mandelbrot_set = MandelbrotSet(max_iterations, escape_radius)

    lines_per_thread = height // n_threads

    # Calcul de l'ensemble de mandelbrot :
    start_time = time()
    with Pool(n_threads) as p:
        convergence = p.starmap(
            get_convergence_fragment,
            list(
                map(
                    lambda rank: (
                        width,
                        lines_per_thread,
                        mandelbrot_set,
                        rank,  # for calculating offset over Y axis
                        n_threads,
                    ),
                    range(n_threads),
                )
            ),
        )
    convergence = np.hstack(convergence)
    end_time = time()
    print(f"Temps du calcul de l'ensemble de Mandelbrot : {end_time-start_time}")

    start_time = time()
    image = get_image_from_convergence_matrix(convergence)
    end_time = time()
    print(f"Temps de constitution de l'image : {end_time-start_time}")
    return image


width, height = 1024, 1024

# Use parallel approach
if len(sys.argv) > 1:
    n_threads = int(sys.argv[1])
    image = get_mandelbrot_image_parallel(width, height, n_threads=n_threads)
# Use original approach
else:
    image = get_mandelbrot_image(width, height)

image = image.convert("RGB")
image.save("mandelbrot.jpg")
image.show()
