import numpy as np
from PIL import Image
import matplotlib.cm
from mpi4py import MPI
from mandelbrot_set import MandelbrotSet
from time import time
import sys

# Source used: https://gist.github.com/mgmarino/2773137

globCom = MPI.COMM_WORLD.Dup()
comm = MPI.COMM_WORLD
n_processes = globCom.size
rank = globCom.rank

n_fragments = n_processes
if len(sys.argv) > 1:
    n_fragments = int(sys.argv[1])


width, height = 1024, 1024
mandelbrot_set = MandelbrotSet(max_iterations=50, escape_radius=10)
lines_per_fragment = height // n_fragments

WORK_TAG = 0
STOP_TAG = 1
DONE_TAG = 2


def get_convergence_fragment(
    fragment_width,
    fragment_height,
    mandelbrot_set,
    current_fragment=0,
):
    convergence = np.empty((fragment_width, fragment_height), dtype=np.double)

    scaleX = 3.0 / width
    scaleY = 2.25 / height

    y_offset = max(current_fragment * fragment_height - 1, 0)

    for y in range(y_offset, y_offset + fragment_height):
        for x in range(fragment_width):
            c = complex(-2.0 + scaleX * x, -1.125 + scaleY * y)
            convergence[x, y - y_offset] = mandelbrot_set.convergence(c, smooth=True)

    return convergence


def get_image_for_mandelbrot_convergence(convergence):
    return Image.fromarray(np.uint8(matplotlib.cm.plasma(convergence.T) * 255))


def master():
    status = MPI.Status()

    tasks = list(
        map(
            lambda n_of_fragment: (width, lines_per_fragment, n_of_fragment),
            range(n_fragments - 1, -1, -1),
        )
    )

    gathered_convergence = [[0] * width] * n_fragments

    for i in range(1, n_processes):
        comm.send(tasks.pop(), dest=i, tag=WORK_TAG)

    while len(tasks) > 0:
        n_of_fragment, data = comm.recv(
            source=MPI.ANY_SOURCE, tag=DONE_TAG, status=status
        )
        gathered_convergence[n_of_fragment] = data
        comm.send(obj=tasks.pop(), dest=status.Get_source(), tag=WORK_TAG)

    for i in range(1, n_processes):
        n_of_fragment, data = comm.recv(source=MPI.ANY_SOURCE, tag=DONE_TAG)
        gathered_convergence[n_of_fragment] = data
        comm.send(obj=None, dest=i, tag=STOP_TAG)

    return np.hstack(gathered_convergence)


def slave():
    status = MPI.Status()

    while True:
        if status.Get_tag() == STOP_TAG:
            break

        (width, height, n_of_fragment) = comm.recv(source=0, tag=WORK_TAG)
        result = get_convergence_fragment(
            fragment_width=width,
            fragment_height=lines_per_fragment,
            mandelbrot_set=mandelbrot_set,
            current_fragment=n_of_fragment,
        )
        comm.send(obj=(n_of_fragment, result), dest=0, tag=DONE_TAG)


if rank == 0:
    start_time = time()
    gathered_convergence = master()
    total_time = time() - start_time
    print(f"Temps du calcul de l'ensemble de Mandelbrot : {total_time} secondes\n")

    start_time = time()
    image = get_image_for_mandelbrot_convergence(gathered_convergence)
    end_time = time()
    print(f"Temps de constitution de l'image : {end_time-start_time} secondes\n")

    image = image.convert("RGB")
    image.save("mandelbrot.jpg")
    image.show()
else:
    slave()
