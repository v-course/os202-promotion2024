import numpy as np
from mpi4py import MPI
from time import time

# Dimension du problème (peut-être changé)
dim = 4000

globCom = MPI.COMM_WORLD.Dup()
comm = MPI.COMM_WORLD
n_processes = globCom.size
rank = globCom.rank

n_fragments = n_processes
columns_per_fragment = dim // n_fragments

n_of_fragment = rank

start_time = time()

A_part = np.array(
    [
        [
            (i + j) % dim + 1.0
            for i in range(
                n_of_fragment * columns_per_fragment,
                (n_of_fragment + 1) * columns_per_fragment,
            )
        ]
        for j in range(dim)
    ]
)

u_part = np.array(
    [
        i + 1.0
        for i in range(
            n_of_fragment * columns_per_fragment,
            (n_of_fragment + 1) * columns_per_fragment,
        )
    ]
)

result_part = A_part.dot(u_part)
current_time = time() - start_time

gathered_result = comm.reduce(result_part, root=0)
subprocesses_total_time = comm.gather(current_time, root=0)

if rank == 0:
    # main process
    print(f"\nv = {gathered_result}")

    print(f"\nTemps du calcul : {np.max(subprocesses_total_time)} secondes")
